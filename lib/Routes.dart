import 'package:flutter/material.dart';
import 'package:Roma/screens/login/index.dart';
import 'package:Roma/screens/register/index.dart';
import 'package:Roma/screens/walkthrough.dart';
import 'package:Roma/screens/home/index.dart';
import 'package:Roma/screens/setting/index.dart';
import 'package:Roma/screens/timeline/index.dart';
import 'package:Roma/screens/list/index.dart';
import 'package:Roma/screens/profile/index.dart';
import 'package:Roma/screens/calender/index.dart';
import 'package:Roma/screens/group/index.dart';
import 'package:Roma/screens/splash/index.dart';
import 'package:flutter/services.dart';

class Routes {
  var routes = <String, WidgetBuilder>{
    "/login": (BuildContext context) => new LoginScreen(),
    "/register": (BuildContext context) => new NewUser(),
    //"/walkthrough": (BuildContext context) => new WalkThrough(),
    "/home": (BuildContext context) => new Home(),
    //"/setting": (BuildContext context) => new SettingScreen(),
    //"/timeline": (BuildContext context) => new TimelineScreen(),
    //"/list": (BuildContext context) => new Items(),
    "/profile": (BuildContext context) => new Profile(),
    //"/calender": (BuildContext context) => new Calender(),
    //"/group": (BuildContext context) => new Group(),
  };
  Routes() {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
        .then((_) {
      runApp(new MaterialApp(
        title: "Flutter Do App",
        debugShowCheckedModeBanner: false,
        home: new Container(child: new Splash()),
        routes: routes,
        onGenerateRoute: (RouteSettings settings) {
          switch (settings.name) {
            case '/login':
              return new MyCustomRoute(
                builder: (_) => new LoginScreen(),
                settings: settings,
              );
//          case '/splash':
//            return new MyCustomRoute(
//              builder: (_) => new Splash(),
//              settings: settings,
//            );
          }
        },
      ));
    });
  }
}

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) {
      return child;
    }
    return new FadeTransition(opacity: animation, child: child);
  }
}
