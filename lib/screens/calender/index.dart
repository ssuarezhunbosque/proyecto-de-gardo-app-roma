import 'package:flutter/material.dart';
import 'package:Roma/components/drawer.dart';
import 'package:Roma/components/datename.dart';
import 'package:Roma/components/calender_card.dart';
import 'package:Roma/components/search.dart';
import 'package:Roma/screens/add_edit/index.dart';
import 'package:Roma/components/buttons.dart';
import 'package:Roma/model/calender_model.dart';
import 'style.dart';

class Calender extends StatefulWidget {
  const Calender({Key key}) : super(key: key);

  @override
  CalenderState createState() => new CalenderState();
}

class CalenderState extends State<Calender> {
  DateTime date = new DateTime.now();
  var list = new DataListBuilder().cardList;
  var monthList = new MonthListBuilder().cardList;
  var weekList = new WeekListBuilder().cardList;
  final TextEditingController searchItem = new TextEditingController();
  filterList(item) {
    setState(() {
      list = new DataListBuilder().cardList;
      weekList = new WeekListBuilder().cardList;
      monthList = new MonthListBuilder().cardList;
      if (item == '') {} else {
        list = list.where((i) => i.task.toLowerCase().contains(item)).toList();
        weekList =
            weekList.where((i) => i.task.toLowerCase().contains(item)).toList();
        monthList = monthList
            .where((i) => i.task.toLowerCase().contains(item))
            .toList();
      }
    });
  }

  onSubmit(item) {
    setState(() {
      if (item == '') {
        search = false;
      }
      filterList(item.toString().toLowerCase());
    });
  }

  onChanged(data) {
    setState(() {
      filterList(data.toString().toLowerCase());
    });
  }

  navigate(type) {
    Navigator.of(context).push(new PageRouteBuilder(
          pageBuilder: (_, __, ___) => new AddEdit(id: type),
        ));
  }

  bool search = false;
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new DefaultTabController(
      length: 3,
      child: new Scaffold(
        floatingActionButton: new TopFloatButton(
          icon: Icons.add,
          navigate: () => navigate(''),
          top: screenSize.height / 2.9,
        ),
        resizeToAvoidBottomPadding: false,
        drawer: new CustomDrawer(),
        appBar: new PreferredSize(
          preferredSize: new Size(screenSize.width, screenSize.height / 3),
          child: new Container(
            alignment: Alignment.topLeft,
            decoration: decoration,
            child: new AppBar(
              flexibleSpace: new Container(
                  alignment: Alignment.centerLeft,
                  padding: const EdgeInsets.only(
                    left: 20.0,
                  ),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        DateName.days[date.weekday - 1],
                        style: title,
                      ),
                      new Text(
                          '${DateName.month[(date.month) - 1]}  ${date.day}, ${date.year}',
                          style: subTitle)
                    ],
                  )),
              backgroundColor: Colors.transparent,
              actions: <Widget>[
                search
                    ? new Expanded(
                        child: new Container(
                          padding: new EdgeInsets.only(top: 8.0, bottom: 2.0),
                          margin: new EdgeInsets.only(
                              bottom: 5.0, top: 2.0, left: 45.0),
                          child: new Search(
                            submitSearch: (item) => onSubmit(item),
                            changeSearch: (item) => onChanged(item),
                          ),
                        ),
                      )
                    : new IconButton(
                        icon: new Icon(
                          Icons.search,
                          size: 25.0,
                        ),
                        onPressed: () => setState(() {
                              search = true;
                            })),
              ],
              bottom: new PreferredSize(
                preferredSize: new Size(10.0, 10.0),
                child: new Container(
                  padding: const EdgeInsets.only(
                      bottom: 0.0, left: 10.0, right: 92.0),
                  child: new TabBar(
                    indicatorColor: Colors.white,
                    indicatorPadding: const EdgeInsets.only(top: 1.0),
                    tabs: [
                      new Tab(
                        child: new Container(
                          child: new Text(
                            'DAY',
                            style: new TextStyle(fontSize: 15.0),
                          ),
                          margin: const EdgeInsets.only(top: 10.0),
                        ),
                      ),
                      new Tab(
                        child: new Container(
                          child: new Text(
                            'WEEK',
                            style: new TextStyle(fontSize: 15.0),
                          ),
                          margin: const EdgeInsets.only(top: 10.0),
                        ),
                      ),
                      new Tab(
                        child: new Container(
                          child: new Text(
                            'MONTH',
                            style: new TextStyle(fontSize: 15.0),
                          ),
                          margin: const EdgeInsets.only(top: 10.0),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        body: new TabBarView(
          children: [
            new CalenderCard(
              list: list,
              type: 'date',
            ),
            new CalenderCard(
              list: weekList,
              type: 'week',
            ),
            new CalenderCard(
              list: monthList,
              type: 'MONTH',
            )
          ],
        ),
      ),
    );
  }
}
