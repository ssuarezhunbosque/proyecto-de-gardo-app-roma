import 'package:flutter/material.dart';

TextStyle title =
    new TextStyle(color: Colors.white, fontSize: 35.0, fontFamily: 'Avenir');

TextStyle subTitle =
    new TextStyle(color: Colors.white, fontSize: 15.0, fontFamily: 'Avenir');
BoxDecoration decoration = new BoxDecoration(
  image: new DecorationImage(
    image: new AssetImage("assets/calender_bg.jpg"),
    fit: BoxFit.cover,
  ),
);
