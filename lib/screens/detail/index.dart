import 'package:flutter/material.dart';
import 'package:Roma/components/drawer.dart';
import 'package:Roma/components/custom_header.dart';
import 'package:Roma/components/profile.dart';
import 'package:Roma/components/buttons.dart';
import 'package:Roma/components/date_time_picker.dart';
import 'package:Roma/components/add-edit/add_edit_card.dart';
import 'package:Roma/model/data.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'style.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Detail extends StatefulWidget {
  final DocumentSnapshot circular;
  const Detail({Key key, this.circular}) : super(key: key);
  @override
  DetailState createState() => new DetailState(circular: this.circular);
}

class DetailState extends State<Detail> {

  DocumentSnapshot circular;
  ///Constructor
  DetailState({this.circular});

  closeKeyboard() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  ///View
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Stack(
      children: <Widget>[
        new Scaffold(
          primary: true,
          drawer: new CustomDrawer(),
          body: new Column(
            children: <Widget>[
              new CustomHeader(
                iconTL: new IconButton(
                  icon: new Icon(
                    Icons.clear,
                    size: 30.0,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    closeKeyboard();
                    new Timer(Duration(milliseconds: 100), () {
                      Navigator.pop(context);
                    });
                  },
                ),
                iconTR: new Container(
                    padding: new EdgeInsets.only(right: 15.0),
                    child: new ProfilePic()),
                title: circular['title'],
                subTitle: '',
                bg: 'assets/task_bg.jpg',
              ),

              new Expanded(
                child: new ListView(
                  children: <Widget>[
                    new Column(
                      //  padding: new EdgeInsets.only(top: 5.0),
                      children: <Widget>[
                        new Container(
                          padding: new EdgeInsets.only(left: 15.0, right: 15.0),
                          margin: const EdgeInsets.only(bottom: 6.0),
                          child: new Text(
                            circular['description'],
                            textAlign: TextAlign.left,
                            style: titleStyle,
                          ),
                        ),
                        /*new Container (
                          padding: new EdgeInsets.only(left: 15.0, right: 15.0),
                          child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new Text(circular['description'], style: subTitle),
                              ]
                          ),
                        ),*/
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
