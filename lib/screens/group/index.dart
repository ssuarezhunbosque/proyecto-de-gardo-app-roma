import 'package:flutter/material.dart';
import 'package:Roma/components/drawer.dart';
import 'package:Roma/components/buttons.dart';
import 'package:Roma/components/search.dart';
import 'package:Roma/model/group_model.dart';
import 'style.dart';
import 'dart:math';

class Group extends StatefulWidget {
  @override
  GroupState createState() => new GroupState(flag: true,list:new DataListBuilder().cardList);
}

class GroupState extends State<Group> {
  GroupState({this.flag,this.list});
  var list = new DataListBuilder().cardList;
  bool search = false;
  double resizeContainer = 1.0;
  int count;
  int itemCount = 99;
  PageController controller;
  static int currentPage;
  bool flag = true;
  bool found = true;
  filter(data) {
    list = new DataListBuilder().cardList;
    itemCount = 99;
    setState(() {
      found = true;
      list = list.where((i) => i.title.toLowerCase().contains(data)).toList();
      count = list.length;
      itemCount = count;
      if (count == 0) {
        list = new DataListBuilder().emptyList;
        count = 1;
        itemCount = 1;
        found = false;
      }
    });
  }

  @override
  initState() {
    super.initState();
    list = new DataListBuilder().cardList;
    flag = true;
    count = list.length;
    currentPage = 50;
    controller = new PageController(
      initialPage: currentPage,
      keepPage: true,
      viewportFraction: 0.8,
    );
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      drawer: new CustomDrawer(),
      resizeToAvoidBottomPadding: false,
      body: new Container(
        decoration: decoration,
        child: new Container(
          margin: const EdgeInsets.only(top: 25.0),
          width: screenSize.width,
          child: new Column(
            children: <Widget>[
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new DrawerButton(),
                  search
                      ? new Expanded(
                          child: new Search(
                            changeSearch: (data) =>
                                filter(data.toString().toLowerCase()),
                            submitSearch: (data) {
                              if (data == '')
                                setState(() {
                                  itemCount = 99;
                                  search = false;
                                });
                              else {
                                filter(data.toString().toLowerCase());
                              }
                            },
                          ),
                        )
                      : new SearchButton(
                          pressed: () => setState(() {
                                search = true;
                              }),
                        ),
                ],
              ),
              new Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.only(left: 25.0),
                child: new Text(
                  'Groups',
                  style: title,
                ),
              ),
              new Container(
                height: (screenSize.height / 1.4),
                child: new PageView.builder(
                    controller: controller,
                    itemCount: itemCount,
                    onPageChanged: (i) => setState(() {
                          currentPage = i;
                        }),
                    itemBuilder: (context, index) => builder(index)),
              ),
              new Center(
                child: new Container(
                    alignment: Alignment.topLeft,
                    height: 4.0,
                    width: (100.0),
                    color: const Color.fromRGBO(250, 250, 250, 0.2),
                    padding: new EdgeInsets.only(
                      left: (100.0 / count.toDouble()) * (currentPage % 5) + 1,
                    ),
                    child: new Container(
                      width: (100.0 / count),
                      color: Colors.white,
                    )),
                //   ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  builder(int index) {
    return new AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        double value;
        if (flag == false && controller != null)
          value = controller.page - index;
        if (index == currentPage - 1 && flag) value = 1.0;
        if (index == currentPage && flag) value = 0.0;
        if (index == currentPage + 1 && flag) {
          value = 1.0;
          flag = false;
        }
        value = (1 - (value.abs() * .2)).clamp(0.0, 1.0);
        return new InkWell(
          onTap: () => Navigator.pushNamed(context, "/list"),
          child: new Opacity(
            opacity: pow(value, 4),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Center(
                  child: new Container(
                    height: value * 300,
                    width: value * 900,
                    decoration: (cardPic(list[index % 5].pic, found)),
                    alignment: Alignment.center,
                    child: !found
                        ? new Text(
                            list[index % count].title,
                            style: cardTitle,
                          )
                        : new Container(),
                  ),
                ),
                found
                    ? new Center(
                        child: new Container(
                          height: value * 100.0,
                          width: value * 900,
                          color: Colors.white,
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              new Text(
                                '${(currentPage.toInt()%5)+1}/$count',
                                style: subTitle,
                              ),
                              new Text(
                                list[index % 5].title,
                                style: cardTitle,
                              ),
                              new Icon(
                                Icons.fiber_manual_record,
                                size: 10.0,
                                color: const Color.fromRGBO(80, 210, 194, 1.0),
                              )
                            ],
                          ),
                        ),
                      )
                    : new Container(),
              ],
            ),
          ),
        );
      },
    );
  }
}
