import 'package:flutter/material.dart';

TextStyle subHeader = new TextStyle(
  fontSize: 15.0,
  color: Colors.white,
);
TextStyle title =
    new TextStyle(fontSize: 35.0, color: Colors.white, fontFamily: 'avenir');
TextStyle cardTitle =
    new TextStyle(fontSize: 30.0, color: Colors.black, fontFamily: 'avenir');
TextStyle subTitle =
    new TextStyle(fontSize: 15.0, color: Colors.black, fontFamily: 'avenir');
BoxDecoration decoration = new BoxDecoration(
  image: new DecorationImage(
    image: new AssetImage("assets/group_bg.jpg"),
    fit: BoxFit.cover,
  ),
);
cardPic(pic, found) {
  return new BoxDecoration(
      image: (found
          ? new DecorationImage(
              image: new AssetImage(pic),
              fit: BoxFit.cover,
            )
          : null),
      color: Colors.white);
}
