import 'package:flutter/material.dart';
import 'package:Roma/model/data.dart';
import 'package:Roma/screens/add_edit/index.dart';
import 'package:Roma/screens/detail/index.dart';
import 'package:Roma/components/drawer.dart';
import 'package:Roma/components/profile.dart';
import 'package:Roma/components/datename.dart';
import 'package:Roma/components/buttons.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'style.dart';
import 'package:date_util/date_util.dart';

class Home extends StatefulWidget {
  //Constructor of the current class
  const Home({
    Key key,
    this.user
  }) : super(key: key);

  final FirebaseUser user;

  @override
  HomeState createState() => new HomeState(user : user);
}

class HomeState extends State<Home> {

  final FirebaseUser user;
  DateTime _date = new DateTime.now();
  var dateUtility = new DateUtil();
  var _temp = '32';
  var _country = 'San Francisco';
  var _list = new DataListBuilder().cardList;


  ///Constructor
  HomeState({Key key, this.user});

  /*@override
  void initState(){
    super.initState();
    fillCircularsArray();
  }*/

  getParentInformation() async {
    //var parent_document = await Firestore.instance.collection('schools').document('ZLMaXPpqg0sIx4sNsbgD').collection('parents').document('4pSuQrPYVI7SPRkFMTXw').get();
    var currentUser = await FirebaseAuth.instance.currentUser();
    var parent_document = await Firestore.instance.collection('schools').document('ZLMaXPpqg0sIx4sNsbgD').collection("parents").document(currentUser.uid).get();
    return parent_document;
  }

  Future fillCircularsArray() async{
    List<DocumentSnapshot> circularList = new List<DocumentSnapshot>();
    var parent = await getParentInformation();
    var parent_data = parent.data;
    var circulars = parent_data['circulars'];
    for (var circular in circulars) {
      var circular_odj = await circular.get();
      circularList.add(circular_odj);
    }
    circularList.sort((a, b) => b['date_time_publication'].toDate().compareTo(a['date_time_publication'].toDate()));
    return circularList;
  }

  navigate(circular) {
    Navigator.of(context).push(new PageRouteBuilder(
          pageBuilder: (_, __, ___) => new Detail(circular: circular),
        ));
  }

  /*navigate(id) {
    Navigator.of(context).push(new PageRouteBuilder(
      pageBuilder: (_, __, ___) => new AddEdit(id: id),
    ));
  }*/

  ///Get data of a document in Firestore
  Widget _buildListItem() {
    return FutureBuilder(
      future: fillCircularsArray(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done){
          if(snapshot.hasError){
            print('error');
          }
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              DocumentSnapshot circular = snapshot.data[index];
              var publication_date = circular['date_time_publication'].toDate();
              var publication_day = publication_date.day.toString();
              var publication_month = dateUtility.month(publication_date.month).substring(0,3);
              return new Container(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: new InkWell(
                  onTap: () => navigate(circular),
                  //onTap: () => navigate(index.toString()),
                  child: new Row(
                    children: <Widget>[
                      ///Fecha del mensaje
                      new Container(
                        width: 40.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              margin:
                              const EdgeInsets.only(bottom: 6.0),
                              child: new Text(
                                publication_day,
                                textAlign: TextAlign.left,
                                style: header,
                              ),
                            ),
                            new Container(
                              margin:
                              const EdgeInsets.only(bottom: 6.0),
                              child: new Text(
                                publication_month,
                                textAlign: TextAlign.left,
                                style: itemStyle,
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Expanded(
                        child: new Container(
                          margin: const EdgeInsets.only(left: 15.0),
                          padding: const EdgeInsets.only(bottom: 20.0, top: 20.0),
                          decoration: (index !=  snapshot.data.length - 1 ? border : null),
                          child: new Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.start,
                            ///Titulo y descripción
                            children: <Widget>[
                              new Container(
                                margin:
                                const EdgeInsets.only(bottom: 6.0),
                                child: new Text(
                                  circular['title'],
                                  textAlign: TextAlign.left,
                                  style: itemStyle,
                                ),
                              ),
                              new Text(
                                circular['description'],
                                textAlign: TextAlign.left,
                                style: subtitle,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        }else{
          return LinearProgressIndicator();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Stack(
      children: <Widget>[
        new Container(
          decoration: decoration,
        ),
        new Scaffold(
          backgroundColor: Colors.transparent,
          drawer: new CustomDrawer(),
          body: new Container(
            padding: const EdgeInsets.only(top: 25.0, left: 10.0, right: 15.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new DrawerButton(),
                    new ProfilePic(),
                  ],
                ),
                new Container(
                  margin:
                      const EdgeInsets.only(top: 25.0, left: 15.0, right: 15.0),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(DateName.days[_date.weekday - 1],
                              textAlign: TextAlign.left, style: title),
                          new Text(
                            '${DateName.month[(_date.month) - 1]}  ${_date.day}, ${_date.year}',
                            textAlign: TextAlign.left,
                            style: subtitle,
                          )
                        ],
                      ),
                      //Temperatura y ciudad
                      /*new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Container(
                                margin: new EdgeInsets.only(
                                    bottom: 8.0, right: 10.0),
                                child: new Icon(
                                  Icons.wb_sunny,
                                  size: 28.0,
                                  color: Colors.white,
                                ),
                              ),
                              new Text(_temp,
                                  textAlign: TextAlign.left, style: title),
                              new Container(
                                padding: const EdgeInsets.only(bottom: 30.0),
                                alignment: Alignment.topRight,
                                child: new Icon(
                                  Icons.radio_button_unchecked,
                                  size: 10.0,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          new Text(
                            '$_country',
                            textAlign: TextAlign.left,
                            style: subtitle,
                          )
                        ],
                      ),*/
                    ],
                  ),
                ),
                new Container(
                  height: screenSize.height / 1.7,
                  margin: const EdgeInsets.only(top: 15.0),
                  ///Listvew builder
                  child: _buildListItem(),
                  /*child: new ListView.builder(
                    itemCount: _list.length,
                    itemBuilder: (context, int index) {
                      return new Container(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: new InkWell(
                          onTap: () => navigate(index.toString()),
                          child: new Row(
                            children: <Widget>[
                              ///Fecha del mensaje
                              new Container(
                                width: 40.0,
                                child: new Column(
                                  children: <Widget>[
                                    new Container(
                                      margin:
                                      const EdgeInsets.only(bottom: 6.0),
                                        child: new Text(
                                          _list[index].date,
                                        textAlign: TextAlign.left,
                                        style: header,
                                      ),
                                    ),
                                    new Text(
                                      _list[index].meridian,
                                      textAlign: TextAlign.left,
                                      style: subHeader,
                                    )
                                  ],
                                ),
                              ),
                              new Expanded(
                                child: new Container(
                                  margin: const EdgeInsets.only(left: 15.0),
                                  padding: const EdgeInsets.only(
                                      bottom: 20.0, top: 20.0),
                                  decoration: (index != _list.length - 1
                                      ? border
                                      : null),
                                  child: new Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    ///Titulo y descripción
                                    children: <Widget>[
                                      new Container(
                                        margin:
                                            const EdgeInsets.only(bottom: 6.0),
                                        child: new Text(
                                          _list[index].task,
                                          textAlign: TextAlign.left,
                                          style: itemStyle,
                                        ),
                                      ),
                                      new Text(
                                        _list[index].source,
                                        textAlign: TextAlign.left,
                                        style: subtitle,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),*/
                ),
                ///Botón para agregar nota
                /*new Expanded(
                  child: new Container(
                    alignment: Alignment.bottomRight,
                    margin: const EdgeInsets.only(bottom: 20.0),
                    child: new FloatButton(
                      icon: Icons.add,
                      navigate: () => navigate(''),
                    ),
                  ),
                ),*/
              ],
            ),
          ),
        ),
      ],
    );
  }
}
