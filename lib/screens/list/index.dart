import 'package:flutter/material.dart';
import 'package:Roma/components/drawer.dart';
import 'package:Roma/components/custom_header.dart';
import 'package:Roma/components/add_item.dart';
import 'package:Roma/components/search.dart';
import 'package:Roma/components/buttons.dart';
import 'package:Roma/model/list_model.dart';
import 'style.dart';

class Items extends StatefulWidget {
  const Items({Key key}) : super(key: key);

  @override
  ItemsState createState() => new ItemsState();
}

class ItemsState extends State<Items> {
  var list = new ItemListBuilder().cardList;
  int completed;
  bool search = false;
  bool add = false;
  double opacity = 1.0;
  dynamic color = const Color.fromRGBO(0, 0, 0, 0.5);
  dynamic icon = Icons.panorama_fish_eye;
  changeList(data) {
    data = data.toString().toLowerCase();
    list = new ItemListBuilder().cardList;
    setState(() {
      list = list.where((i) => i.item.toLowerCase().contains(data)).toList();
    });
  }

  onSubmit(data) {
    data = data.toString().toLowerCase();
    list = new ItemListBuilder().cardList;
    setState(() {
      if (data == '') {
        search = false;
      } else {
        list = list.where((i) => i.item.toLowerCase().contains(data)).toList();
      }
    });
  }

  select(item) {
    setState(() {
      item.complted = !item.complted;
      if (item.complted == true) {
        item.icon = Icons.check;
        item.color = Colors.green;
      } else {
        item.icon = Icons.panorama_fish_eye;
        item.color = const Color.fromRGBO(0, 0, 0, 0.5);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    completed = 0;
    list.forEach((i) => completed = i.complted ? completed + 1 : completed);
    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      drawer: new CustomDrawer(),
      floatingActionButton: (!add
          ? new TopFloatButton(
              icon: Icons.add,
              top: (screenSize.height / 3.8),
              navigate: () => setState(() {
                    add = true;
                    opacity = 0.2;
                    FocusScope.of(context).requestFocus(new FocusNode());
                    list = new ItemListBuilder().cardList;
                    search = false;
                  }))
          : null),
      resizeToAvoidBottomPadding: false,
      body: new Stack(
        children: <Widget>[
          new Opacity(
            opacity: opacity,
            child: new Column(
              children: <Widget>[
                new CustomHeader(
                  iconTL: new IconButton(
                    icon: new Icon(
                      Icons.arrow_back,
                      size: 25.0,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  iconTR: search
                      ? new Expanded(
                          child: new Search(
                            changeSearch: (item) {
                              changeList(item);
                            },
                            submitSearch: (item) {
                              onSubmit(item);
                            },
                          ),
                        )
                      : new SearchButton(
                          pressed: () => setState(() {
                                search = true;
                              }),
                        ),
                  title: 'Salad',
                  subTitle: '',
                  bg: 'assets/list_bg.jpg',
                ),
                new Container(
                  height: screenSize.height / 1.4,
                  width: screenSize.width,
                  child: new ListView.builder(
                      padding: const EdgeInsets.only(top: 0.0),
                      itemCount: list.length,
                      itemBuilder: (context, int i) {
                        return new Container(
                            padding: const EdgeInsets.only(
                              top: 10.0,
                              bottom: 10.0,
                            ),
                            margin:
                                new EdgeInsets.only(left: 10.0, right: 10.0),
                            decoration: (i < list.length - 1 ? border : null),
                            child: new Row(
                              children: <Widget>[
                                new Container(
                                  width: 40.0,
                                  child: new IconButton(
                                      icon: new Icon(
                                        list[i].icon,
                                        size: 20.0,
                                      ),
                                      color: list[i].color,
                                      onPressed: () => select(list[i])),
                                ),
                                new Container(
                                    margin: const EdgeInsets.only(left: 15.0),
                                    child: new Text(
                                      list[i].item,
                                      style: textStyle,
                                    ))
                              ],
                            ));
                      }),
                ),
              ],
            ),
          ),
          add
              ? new AddItem(save: () {
                  setState(() {
                    add = false;
                    opacity = 1.0;
                  });
                })
              : new Container()
        ],
      ),
    );
  }
}
