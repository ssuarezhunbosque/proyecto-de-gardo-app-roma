import 'package:flutter/material.dart';

TextStyle textStyle =
    new TextStyle(fontSize: 20.0, color: Colors.black, fontFamily: 'Avenir');
BoxDecoration border = new BoxDecoration(
    border: new Border(
        bottom: new BorderSide(
            color: const Color.fromRGBO(0, 0, 0, 0.2),
            width: 1.0),
    ),
);