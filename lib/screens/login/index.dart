import 'package:Roma/screens/home/index.dart';
import 'package:flutter/material.dart';
import 'package:Roma/services/validation.dart';
import 'package:Roma/components/text_field.dart';
import 'package:Roma/components/buttons.dart';
import 'package:flutter/scheduler.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'style.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({
    Key key,
  })
      : super(key: key);

  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  final TextEditingController _username = new TextEditingController();
  final TextEditingController _password = new TextEditingController();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool autoValidate = false;
  Validations validations = new Validations();

  Future _handleSignIn() async {
    final FormState form = formKey.currentState;
    setState(() {
      autoValidate = true;
    });
    if (!form.validate()) {
      showInSnackBar('Please fix the errors in red before submitting.');
    } else {
      form.save();
      signIn(_username.text, _password.text);
    }
  }

  //Future<void>signIn(String email, String password) async{}
  ///SignIn with Flutter and Firebase logic
  Future<void>signIn(String email, String password) async{
    try{
      final FirebaseAuth _auth = FirebaseAuth.instance;
      final FirebaseUser user = (await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      )).user;
      var token = await user.getIdToken();
      print('Me he logueado, esre es mi uid:  '+ user.uid);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Home(user: user)),
      );
    }catch(e){
      showInSnackBar(e.message);
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(milliseconds: 700),
      backgroundColor: new Color.fromRGBO(107, 85, 153, 1.0),
    ));
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 1.2;
    final Size screenSize = MediaQuery.of(context).size;
    return new Stack(
      children: <Widget>[
        new Container(
          height: screenSize.height,
          width: screenSize.width,
          decoration: background,
        ),
        new Scaffold(
            backgroundColor: Colors.transparent,
            key: _scaffoldKey,
            body: new ListView(
              padding: new EdgeInsets.all(0.0),
              children: <Widget>[
                new Form(
                  key: formKey,
                  autovalidate: autoValidate,
                  child: new Container(
                    height: screenSize.height,
                    width: screenSize.width,
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new Center(
                            child: new Hero(
                          tag: 'tick',
                          child: new Image.asset('assets/mark.png',
                              width: 180.0, height: 180.0, scale: 1.0),
                        )),
                        new InputField(
                            icon: Icons.person_outline,
                            hint: 'Email',
                            protected: false,
                            type: TextInputType.emailAddress,
                            controller: _username,
                            validate: validations.validateEmail),
                        new InputField(
                          icon: Icons.lock_outline,
                          hint: 'Password',
                          protected: true,
                          controller: _password,
                          validate: validations.validatePassword,
                        ),
                        new Column(
                          children: <Widget>[
                            new Container(
                              margin: new EdgeInsets.only(bottom: 25.0),
                              child: new Center(
                                  child: new RoundButton(
                                height: 60.0,
                                width: screenSize.width - 100.0,
                                color: const Color.fromRGBO(255, 51, 102, 1.0),
                                text: 'Login',
                                onPressed: () {
                                  _handleSignIn();
                                },
                              )),
                            ),
                            new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Text("Don't have an Account?  ",
                                      style: new TextStyle(
                                          color: new Color.fromRGBO(
                                              255, 255, 255, 80.0),
                                          fontSize: 12.0)),
                                  new InkWell(
                                    onTap: () {
                                      Navigator.pushNamed(context, "/register");
                                    },
                                    child: new Padding(
                                      padding: new EdgeInsets.only(
                                          top: 10.0,
                                          bottom: 10.0,
                                          left: 3.0,
                                          right: 10.0),
                                      child: new Text("Sign Up",
                                          style: new TextStyle(
                                              color: Colors.white,
                                              fontSize: 14.0)),
                                    ),
                                  ),
                                ]),
                            new Container(
                              alignment: Alignment.topRight,
                              margin: new EdgeInsets.only(right: 15.0),
                              child: new InkWell(
                                  onTap: () {
                                    Navigator.pushNamed(
                                        context, "/home");
                                  },
                                  child: new Container(
                                      padding: new EdgeInsets.only(
                                          left: 10.0,
                                          right: 10.0,
                                          top: 5.0,
                                          bottom: 5.0),
                                      decoration: border,
                                      child:
                                          new Text('Skip', style: skipStyle)
                                  )
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )),
      ],
    );
  }
}
