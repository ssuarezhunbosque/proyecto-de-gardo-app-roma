import 'package:flutter/material.dart';
import 'package:Roma/components/drawer.dart';
import 'package:Roma/components/buttons.dart';
import 'package:Roma/components/circular_chart.dart';
import 'package:Roma/model/data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'style.dart';

class Profile extends StatefulWidget {
  const Profile({Key key}) : super(key: key);

  @override
  ProfileState createState() => new ProfileState();
}

class ProfileState extends State<Profile> {

  var parent_information = null;

  var list = new ItemListBuilder().cardList;
  var item = new DateListBuilder().dateList;
  static int i = 0;

  @override
  void initState(){
    super.initState();
    getParentInformation();
  }

  void getParentInformation() async {
    var currentUser = await FirebaseAuth.instance.currentUser();
    var parent_document = await Firestore.instance.collection('schools').document('ZLMaXPpqg0sIx4sNsbgD').collection("parents").document(currentUser.uid).get();
    this.parent_information = parent_document.data;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if(parent_information != null){
      var birthday_date = parent_information['birthday'].toDate();
      var birthday = birthday_date.toString().substring(0,10);
      final Size screenSize = MediaQuery
          .of(context)
          .size;
      return new Scaffold(
        drawer: new CustomDrawer(),
        body: new Container(
          child: new Column(
            children: <Widget>[
              new Container(
                height: screenSize.height / 2.2,
                width: screenSize.width,
                padding: const EdgeInsets.only(right: 15.0, top: 25.0),
                decoration: decoration,
                child: new Column(
                  children: <Widget>[
                    new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new DrawerButton(),
                          new Container(
                              alignment: Alignment.topRight,
                              height: 35.0,
                              width: 35.0,
                              padding:
                              new EdgeInsets.only(left: 10.0, top: 5.0),
                              margin: new EdgeInsets.only(top: 10.0),
                              decoration:
                              pic,
                              child: new Icon(
                                Icons.fiber_manual_record,
                                color: Colors.red,
                                size: 10.0,
                              )
                          ),
                        ]
                    ),
                    new Container(
                      padding: const EdgeInsets.only(left: 25.0, top: 100.0),
                      alignment: Alignment.topLeft,
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            '${parent_information['first_name']} ${parent_information['last_name']}',
                            //style: appTitle,
                            style: TextStyle(fontSize: 40.0,
                                fontFamily: 'Avenir',
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                height: screenSize.height * (1.2 / 2.2) - 80,
                width: screenSize.width,
                margin: new EdgeInsets.only(left: 10.0, right: 10.0),
                child: new ListView(
                  children: <Widget>[
                    new Column(
                      children: <Widget>[
                        //email
                        new Row(
                          children: <Widget>[
                            new Text(
                              'Email: ',
                              textAlign: TextAlign.left,
                              style: TextStyle(fontSize: 20.0,
                                  fontFamily: 'Avenir',
                                  color: Colors.black),
                            ),
                            new Text(
                              parent_information['email'],
                              style: TextStyle(fontSize: 18.0,
                                  fontFamily: 'Avenir',
                                  color: Colors.black),
                            ),
                          ],
                        ),
                        //birthday
                        new Row(
                          children: <Widget>[
                            new Text(
                              'Cumpleaños: ',
                              textAlign: TextAlign.left,
                              style: TextStyle(fontSize: 20.0,
                                  fontFamily: 'Avenir',
                                  color: Colors.black),
                            ),
                            new Text(
                              birthday,
                              style: TextStyle(fontSize: 18.0,
                                  fontFamily: 'Avenir',
                                  color: Colors.black),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    }else{
      return LinearProgressIndicator();
    }
  }
}
