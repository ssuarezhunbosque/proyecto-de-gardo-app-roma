import 'package:flutter/material.dart';
import 'package:Roma/components/text_field.dart';
import 'package:Roma/components/buttons.dart';
import 'package:Roma/components/date_time_picker.dart';
import 'package:Roma/services/validation.dart';
import 'dart:async';
import 'style.dart';

class NewUser extends StatefulWidget {
  const NewUser({Key key}) : super(key: key);
  @override
  NewUserState createState() => new NewUserState();
}

class NewUserState extends State<NewUser> {
  final TextEditingController _name = new TextEditingController();
  final TextEditingController _email = new TextEditingController();
  final TextEditingController _password = new TextEditingController();
  final TextEditingController _birthday = new TextEditingController();
  final GlobalKey<FormState> formKey1 = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String dob;
  bool autoValidate = false;
  Validations validations = new Validations();
  void _handleSignUp() {
    final FormState form = formKey1.currentState;
    setState(() {
      autoValidate = true;
    });
    if (!form.validate()) {
      if (_birthday.text == '') {
        showInSnackBar('Please fill all the field .');
      } else {
        showInSnackBar('Please fix the errors in red before submitting.');
      }
    } else {
      form.save();
      Navigator.pushNamed(context, "/walkthrough");
    }
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      duration: new Duration(milliseconds: 700),
      backgroundColor: new Color.fromRGBO(107, 85, 153, 1.0),
    ));
  }

  closeKeyboard() {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  openCalender() async {
    selectDate(context).then((date) {
      setState(() {
        if (date != null) {
          _birthday.text = date;
          dob = date;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Stack(
      children: <Widget>[
        new Container(
          decoration: background,
        ),
        new Scaffold(
          backgroundColor: Colors.transparent,
          key: _scaffoldKey,
          body: new Form(
            key: formKey1,
            autovalidate: autoValidate,
            child: new ListView(
              padding: new EdgeInsets.only(top: 15.0),
              children: <Widget>[
                new Container(
                  alignment: Alignment.topLeft,
                  child: new IconButton(
                    icon: new Icon(
                      Icons.arrow_back,
                      size: 28.0,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, "/login");
                    },
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.only(left: 30.0, bottom: 10.0),
                  child: new Row(
                    children: <Widget>[
                      new Text(
                        "New Account",
                        style: new TextStyle(
                            fontSize: 31.0,
                            color: Colors.white,
                            fontFamily: 'Avenir'),
                      ),
                    ],
                  ),
                ),
                new Hero(
                  tag: 'tick',
                  child: new Image.asset('assets/mark.png',
                      width: 150.0, height: 150.0, scale: 1.0),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: new InputField(
                      icon: Icons.person_outline,
                      hint: 'Name',
                      protected: false,
                      controller: _name,
                      validate: validations.validateName),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 6.0, bottom: 6.0),
                  child: new InputField(
                      icon: Icons.mail_outline,
                      type: TextInputType.emailAddress,
                      hint: 'Email',
                      protected: false,
                      controller: _email,
                      validate: validations.validateEmail),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 6.0, bottom: 6.0),
                  child: new InputField(
                    icon: Icons.lock_outline,
                    hint: 'Password',
                    protected: true,
                    controller: _password,
                    validate: validations.validatePassword,
                  ),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 6.0, bottom: 6.0),
                  child: new InkWell(
                    onTap: () {
                      closeKeyboard();
                      new Timer(Duration(milliseconds: 100), () {
                        openCalender();
                      });
                    },
                    child: new InputField(
                      icon: Icons.date_range,
                      enable: false,
                      hint: 'Birthday',
                      protected: false,
                      controller: _birthday,
                      validate: validations.validateDate,
                    ),
                  ),
                ),
                new Padding(
                  padding: const EdgeInsets.only(bottom: 8.0, top: 25.0),
                  child: new Center(
                      child: new RoundButton(
                    height: 60.0,
                    width: screenSize.width - 100.0,
                    color: const Color.fromRGBO(255, 51, 102, 1.0),
                    text: 'Create',
                    onPressed: () {
                      _handleSignUp();
                    },
                  )),
                ),
                new Container(
                  alignment: Alignment.centerRight,
                  padding: new EdgeInsets.only(right: 15.0),
                  child: new InkWell(
                      onTap: () => Navigator.pushNamed(context, "/walkthrough"),
                      child: new Container(
                          padding: new EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                          decoration: border,
                          child: new Text('Skip', style: skipStyle))),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
