import 'package:flutter/material.dart';

BoxDecoration border = new BoxDecoration(
  border: new Border.all(color: Colors.white, width: 1.0),
  borderRadius: new BorderRadius.all(const Radius.circular(25.0)),
);
BoxDecoration background = new BoxDecoration(
  image: new DecorationImage(
    image: new AssetImage("assets/sign_up_bg.jpg"),
    fit: BoxFit.cover,
  ),
);
TextStyle skipStyle = new TextStyle(color: Colors.white);
