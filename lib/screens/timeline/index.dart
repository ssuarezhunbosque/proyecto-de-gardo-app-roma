import 'package:flutter/material.dart';
import 'package:Roma/components/drawer.dart';
import 'package:Roma/components/buttons.dart';
import 'package:Roma/components/custom_header.dart';
import 'package:Roma/components/datename.dart';
import 'package:Roma/model/data.dart';
import 'package:Roma/screens/add_edit/index.dart';
import 'package:Roma/components/custom_card.dart';
import 'package:Roma/components/profile.dart';

import 'style.dart';

class TimelineScreen extends StatefulWidget {
  const TimelineScreen({Key key}) : super(key: key);

  @override
  TimelineScreenState createState() => new TimelineScreenState();
}

class TimelineScreenState extends State<TimelineScreen> {
  var _dateList = new DayListBuilder().dateList;
  static DateTime _date = new DateTime.now();
  String subHeader = '${DateName.month[_date.month - 1]} ${_date.year}';
  navigate(type) {
    Navigator.of(context).push(new PageRouteBuilder(
      pageBuilder: (_, __, ___) => new AddEdit(id: type),
    ));
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        drawer: new CustomDrawer(),
        floatingActionButton: new TopFloatButton(
          top: screenSize.height / 4 + 5,
          icon: Icons.add,
          navigate: () => navigate(''),
        ),
        body: new Container(
          child: new Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  new CustomHeader(
                    iconTL: new DrawerButton(),
                    iconTR: new Container(
                        padding: new EdgeInsets.only(right: 15.0),
                        child: new ProfilePic()),
                    title: 'Timeline',
                    subTitle: subHeader,
                    bg: 'assets/timeline_bg.jpg',
                  ),
                  new Container(
                    height: screenSize.height / 1.4,
                    width: screenSize.width,
                    child: new ListView.builder(
                      padding: const EdgeInsets.only(top: 0.0),
                      itemCount: _dateList.length,
                      itemBuilder: (context, int index) {
                        return new Container(
                          color: const Color.fromRGBO(255, 51, 102, 0.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                  width: screenSize.width,
                                  padding: const EdgeInsets.only(
                                      top: 20.0, bottom: 20.0, left: 22.0),
                                  color:
                                      const Color.fromRGBO(220, 220, 220, 1.0),
                                  child: new Text(
                                    _dateList[index].date,
                                    style: new TextStyle(
                                        fontSize: 14.0,
                                        color:
                                            const Color.fromRGBO(0, 0, 0, 0.5),
                                        fontFamily: 'Avenir'),
                                  )),
                              new Stack(
                                children: <Widget>[
                                  new Container(
                                    height: screenSize.height / 2,
                                    margin: const EdgeInsets.only(left: 80.0),
                                    decoration: border,
                                  ),
                                  new Container(
                                    height: screenSize.height * (1 / 2),
                                    width: screenSize.width - 5,
                                    child: new Container(
                                      child: new CustomCard(
                                        list: _dateList[index].list,
                                        textColor: Colors.black,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
