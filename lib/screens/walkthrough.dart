import 'package:flutter/material.dart';
import 'package:Roma/components/buttons.dart';
import 'dart:async';

class WalkThrough extends StatefulWidget {
  const WalkThrough({Key key}) : super(key: key);

  @override
  WalkThroughState createState() => new WalkThroughState();
}

class WalkThroughState extends State<WalkThrough> {
  @override
  initState() {
    super.initState();
    new Future.delayed(const Duration(seconds: 4));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Stack(
      children: <Widget>[
        new Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("assets/walkthrough_bg.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        new Scaffold(
            backgroundColor: Colors.transparent,
            body: new Stack(
              children: <Widget>[
                new Column(
                  children: <Widget>[
                    new Expanded(
                      child: new Container(
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            new Text(
                              "Walkthrough",
                              style: new TextStyle(
                                  fontSize: 35.0,
                                  color: Colors.white,
                                  fontFamily: 'Avenir'),
                            ),
                            new Center(
                              child: new Container(
                                  height: 120.0,
                                  width: 120.0,
                                  decoration: new BoxDecoration(
                                    color: const Color.fromRGBO(
                                        250, 250, 250, 0.1),
                                    borderRadius: new BorderRadius.all(
                                        const Radius.circular(120.0)),
                                  ),
                                  child: new Center(
                                    child: new Icon(
                                      Icons.border_color,
                                      size: 40.0,
                                      color: Colors.white,
                                    ),
                                  )),
                            ),
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Container(
                                  margin: const EdgeInsets.only(bottom: 15.0),
                                  child: new Text(
                                    "The last task management app",
                                    textAlign: TextAlign.center,
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                        fontFamily: 'Avenir'),
                                  ),
                                ),
                                new Text(
                                  "you'll ever need",
                                  textAlign: TextAlign.center,
                                  style: new TextStyle(
                                      color: Colors.white,
                                      fontSize: 15.0,
                                      fontFamily: 'Avenir'),
                                )
                              ],
                            ),
                            new Center(
                              child: new RoundButton(
                                color: const Color.fromRGBO(255, 51, 102, 1.0),
                                height: 65.0,
                                width: screenSize.width / 2.5,
                                onPressed: () {
                                  Navigator.pushNamed(context, "/home");
                                },
                                text: 'Next',
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              ],
            )),
      ],
    );
  }
}
